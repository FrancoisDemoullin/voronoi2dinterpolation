
#include "Shader.h"

class SliceRenderer 
{
public:
	SliceRenderer();
	~SliceRenderer();

	void updateTexture(GLfloat* pNewArray);
	void renderTexture();

private:
	GLuint mVAO;
	GLuint mRenderTextureId;
	GLfloat* mTextureArray;
	Shader* mRenderTextureShader;
	
	void initTexture();
};