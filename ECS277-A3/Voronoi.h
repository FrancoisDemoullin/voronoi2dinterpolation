#pragma once

// Utility includes
#include <cstdio>
#include <vector>
#include <memory>

// my includes
#include "Geometry.h"

// Boost includes
#include <boost/polygon/voronoi.hpp>
using boost::polygon::voronoi_builder;
using boost::polygon::voronoi_diagram;
using boost::polygon::x;
using boost::polygon::y;
using boost::polygon::low;
using boost::polygon::high;

namespace boost
{
	namespace polygon
	{
		template <>
		struct geometry_concept<Point>
		{
			typedef point_concept type;
		};

		template <>
		struct point_traits<Point>
		{
			typedef int coordinate_type;

			static inline coordinate_type get(
				const Point& point,
				orientation_2d orient)
			{
				return (orient == HORIZONTAL) ? point.mX : point.mY;
			}
		};
	}
}

class Voronoi 
{
public:
	Voronoi(std::vector<Point> &pPoints);
	std::vector<Segment> iterateVerticesReturnEdges(Point pP);
	float iterateVerticesReturnValue(Point pP);
	int iterateVerticesReturnIndex(Point pP);
	void handleRequestOverrideMembers(Point pP);

	// getters
	inline Point getLastSite() { return mLastSite; }
	inline Point getLastRequest() { return mLastRequestPoint; }
	inline std::vector<Segment> getLastEdges() { return mLastEdges; }
	inline std::vector<Point> getSurroundingSites() { return mSurroundingSites; }
	inline std::vector<Point> getOriginalPoints() { return mPoints; }
	inline std::vector<float> getWeigths() { return mWeights; }

private:
		
	voronoi_diagram<double> mVd;
	std::vector<Point> & mPoints;
	std::vector<float> mWeights; // follows mPoints indexing!!!

	Point mLastSite;
	Point mLastRequestPoint;
	std::vector<Segment> mLastEdges;

	std::vector<Point> mSurroundingSites;

	vector<Segment> boundary;

	void weighCells();
};

