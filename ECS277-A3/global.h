#pragma once

#include "Geometry.h"

#include <vector>
#include <assert.h>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace global
{
	extern int gScreenHeight;
	extern int gScreenWidth;

	extern float gMinValue;
	extern float gMaxValue;

	extern int gDebuggingInt;

	extern int gMode;

	extern vector<Segment> gBoundary;
	extern vector<Point> gPoints;

	void inline initializeDataSet(GLfloat* pDataSet, std::vector<Point>& pPoints)
	{
		for (int i = 0; i < gScreenHeight * gScreenWidth; i++)
		{
			pDataSet[i] = -1.0f;
		}

		for (auto& currentPoint : pPoints)
		{
			int lX = currentPoint.mX;
			int lIndex = lX + gScreenWidth * currentPoint.mY;
			pDataSet[lIndex] = currentPoint.mValue;
		}
	}

	void inline computeMaxMin(std::vector<Point>& pPoints)
	{
		gMinValue = pPoints.at(0).mValue;
		gMaxValue = pPoints.at(0).mValue;

		for (int i = 1; i < pPoints.size(); i++)
		{
			if (pPoints.at(i).mValue > gMaxValue)
			{
				gMaxValue = pPoints.at(i).mValue;
			}
			else if (pPoints.at(i).mValue < gMinValue)
			{
				gMinValue = pPoints.at(i).mValue;
			}
		}

		assert(gMaxValue > gMinValue);
	}

	inline double polygonArea(const vector<Point>& pVec)
	{
		//assert(pVec.size() == 6 || pVec.size() == 5 || pVec.size() == 4);
		double area = 0.0;
		int j = pVec.size() - 1;

		for (int i = 0; i < pVec.size(); i++)
		{
			area += (pVec[j].mX + pVec[i].mX) * (pVec[j].mY - pVec[i].mY);
			j = i;
		}

		return area * 0.5;
	}

	// returns -1 if not on edge, else returns the edge index
	inline int edgeIntersection(Point pP)
	{
		if (-0.01 < pP.mX && 0.01 > pP.mX)
		{
			if (0.0 <= pP.mY && 600.0 >= pP.mY)
			{
				return 0;
			}
		}
		if (-0.01 < pP.mY && 0.01 > pP.mY)
		{
			if (0.0 <= pP.mX && 600.0 >= pP.mX)
			{
				return 1;
			}
		}
		if (599.99 < pP.mY && 600.01 > pP.mY)
		{
			if (0.0 <= pP.mX && 600.0 >= pP.mX)
			{
				return 2;
			}
		}
		if (599.99 < pP.mX && 600.01 > pP.mX)
		{
			if (0.0 <= pP.mY && 600.0 >= pP.mY)
			{
				return 3;
			}
		}
		return -1;
	}
} // end of namespace