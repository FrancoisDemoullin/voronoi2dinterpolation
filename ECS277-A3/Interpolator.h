#pragma once

#include <memory>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "global.h"
#include "Voronoi.h"

namespace Interpolator 
{
	struct AreaValue
	{
		AreaValue() : area(-1.0), value(-1.0) {}
		AreaValue(float pArea, float pValue) : area(pArea), value(pValue) {}
		float area;
		float value;
	};

	// return read-only raw pointer to mCurrentArray
	void computeTextureArray(int pMode, Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const &gDataSet);
	void stupidInterpolation(Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const &gDataSet);
	float interpolateValue(int xit, int yit, Voronoi * pVD);
	void areaInterpolation(Voronoi * pVD, GLfloat * lArray, size_t lSize, std::unique_ptr<GLfloat[]> const & gDataSet);
	void idiotInterpolation(Voronoi * pVD, GLfloat * lArray, size_t lSize, std::unique_ptr<GLfloat[]> const & gDataSet);
	void simpleSibson(Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const &gDataSet);

	void computeErrorArray(Voronoi * pVD, GLfloat * lArray, size_t lSize, GLfloat * pCompareMe, std::unique_ptr<GLfloat[]> const & gDataSet);
};