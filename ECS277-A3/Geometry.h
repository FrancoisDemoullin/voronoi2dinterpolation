#pragma once

#include <stdlib.h>  // abs
#include <math.h>
#include <assert.h>
#include <vector>
#include <algorithm>  // sort

using namespace std;

struct Point
{
    double mX;
    double mY;

    float mValue;

    const float EPSILON = 0.001f;

    inline Point()
        : mX(0.0), mY(0.0), mValue(0.0) {}
    inline Point(int pX, int pY, float pValue)
        : mX(pX), mY(pY), mValue(pValue) {}
    inline Point(int pX, int pY)
        : mX(pX), mY(pY), mValue(-1.0) {}
    inline Point(double pX, double pY, float pValue)
        : mX(pX), mY(pY), mValue(pValue) {}
    inline Point(double pX, double pY)
        : mX(pX), mY(pY), mValue(-1.0) {}
    inline Point(const Point& pPoint)
    {
        mX = pPoint.mX;
        mY = pPoint.mY;
        mValue = pPoint.mValue;
    }
    void setX(int pX) { mX = pX; }
    void setY(int pY) { mY = pY; }
    void setX(double pX) { mX = pX; }
    void setY(double pY) { mY = pY; }
    void setValue(float pValue) { mValue = pValue; }
    inline float computeDistance(Point pOther)
    {
        return pow(pow(mX - pOther.mX, 2) + pow(mY - pOther.mY, 2), 0.5);
    }
	inline float computeDistanceSquared(Point pOther)
	{
		return (mX - pOther.mX) * (mX - pOther.mX) + (mY - pOther.mY) * (mY - pOther.mY);
	}
    inline void operator=(const Point& pP)
    {
        mX = pP.mX;
        mY = pP.mY;
        mValue = pP.mValue;
    }
    inline bool operator==(const Point& other) const
    {
        const float EPSILON = 0.001f;
        if (abs(other.mX - mX) < EPSILON && abs(other.mY - mY) < EPSILON)
        {
            return true;
        }
        return false;
    }
};

struct Segment
{
    Point mP0;
    Point mP1;

    const float INTERSECTION_EPSILON = 0.3f;

    inline Segment()
        : mP0(-1.0, -1.0), mP1(-1.0, -1.0) {}
    inline Segment(int pX0, int pY0, int pX1, int pY1)
        : mP0(pX0, pY0), mP1(pX1, pY1) {}
    inline Segment(Point pP0, Point pP1)
        : mP0(pP0), mP1(pP1) {}
    inline void operator=(const Segment& pOther)
    {
        mP0 = pOther.mP0;
        mP1 = pOther.mP1;
    }
    bool isOnLine(float pX, float pY);
    float getSlope();
};

struct Line
{
    Point mP0;
    float mSlope;
    float mB;
    bool isInf;

    inline Line(Point pP, float pSlope)
        : mP0(pP), mSlope(pSlope), isInf(false)
    {
        mB = pP.mY - pSlope * pP.mX;
    }
    inline Line(Point pP, float pSlope, bool pDegenerate)
        : mP0(pP), mSlope(pSlope), isInf(pDegenerate)
    {
        if (pDegenerate == false)
        {
            mB = pP.mY - pSlope * pP.mX;
        }
    }

    Point computeIntersection(Line pOther);
    vector<Point> computeIntersections(vector<Segment>& pSeg, vector<int>& pEdgeIndexVector);
	vector<Point> Line::computeBoundaryIntersections();
};
