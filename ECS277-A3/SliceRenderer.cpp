#include "SliceRenderer.h"

#include "global.h"

#include <stdio.h>   
#include <stdlib.h> 

using namespace global;

SliceRenderer::SliceRenderer() 
{
	GLfloat first_pass_vertices[] =
	{
		// Positions          // Tex 
		1.0f,  1.0f, 0.0f,    1.0f, 1.0f, // Top Right
		1.0f, -1.0f, 0.0f,    1.0f, 0.0f, // Bottom Right
		-1.0f, -1.0f, 0.0f,   0.0f, 0.0f, // Bottom Left
		-1.0f,  1.0f, 0.0f,   0.0f, 1.0f  // Top Left 
	};

	GLuint indices[] = 
	{
		0, 1, 3, // First Triangle
		1, 2, 3  // Second Triangle
	};

	glGenVertexArrays(1, &mVAO);
	glBindVertexArray(mVAO);

	GLuint firstPassVBO;
	glGenBuffers(1, &firstPassVBO);
	glBindBuffer(GL_ARRAY_BUFFER, firstPassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(first_pass_vertices), first_pass_vertices, GL_STATIC_DRAW);

	GLuint firstPassEBO;
	glGenBuffers(1, &firstPassEBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, firstPassEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Textures
	initTexture();

	// array that is at the basis of the texture
	size_t lSizeOfRayArray = global::gScreenHeight * global::gScreenWidth * 3 * sizeof(GLfloat);
	mTextureArray = (GLfloat *) malloc(lSizeOfRayArray);

	mRenderTextureShader = new Shader("DebuggingVert.glsl", "DebuggingFrag.glsl");
}

SliceRenderer::~SliceRenderer()
{
	//mRenderTextureShader->~Shader();
}

void SliceRenderer::initTexture() 
{
	glEnable(GL_TEXTURE_2D);

	glGenTextures(1, &mRenderTextureId);
	glBindTexture(GL_TEXTURE_2D, mRenderTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, global::gScreenHeight, global::gScreenWidth, 0, GL_RGB, GL_FLOAT, mTextureArray);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindVertexArray(0);
}

void SliceRenderer::updateTexture(GLfloat* pNewArray)
{
	glBindTexture(GL_TEXTURE_2D, mRenderTextureId);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, global::gScreenHeight, global::gScreenWidth, GL_RGB, GL_FLOAT, pNewArray);
}

void SliceRenderer::renderTexture()
{
	mRenderTextureShader->use();

	glViewport(0, 0, global::gScreenWidth, global::gScreenHeight);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mRenderTextureId);

	glUniform1i(glGetUniformLocation(mRenderTextureShader->mProgram, "debugMe"), 0);

	glBindVertexArray(mVAO);
	
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
}
