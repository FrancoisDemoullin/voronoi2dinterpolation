#include "Interpolator.h"

#include <iostream>

void Interpolator::computeTextureArray(int pMode, Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const& gDataSet)
{
    if (pMode == 0)
    {
        stupidInterpolation(pVD, lArray, lSize, gDataSet);
    }
    else if (pMode == 1)
    {
        simpleSibson(pVD, lArray, lSize, gDataSet);
    }
    else if (pMode == 2)
    {
        idiotInterpolation(pVD, lArray, lSize, gDataSet);
    }
	else 
	{
		assert(1 == 0);
	}
}

void Interpolator::stupidInterpolation(Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const& gDataSet)
{
#pragma omp parallel for
    for (int xit = 0; xit < global::gScreenWidth; xit++)
    {
#pragma omp parallel for
        for (int yit = 0; yit < global::gScreenWidth; yit++)
        {
            int lFlatIndex = xit % global::gScreenWidth + global::gScreenWidth * yit;
            int lFlatIndexRGB = 3 * lFlatIndex;

            float lValue;

            if (gDataSet[lFlatIndex] < 0.0f)
            {
                // this value needs to be interpolated
                float lSiteValue = pVD->iterateVerticesReturnValue(Point(xit, yit));
                lValue = (lSiteValue - global::gMinValue) / (global::gMaxValue - global::gMinValue);
            }
            else
            {
                lValue = (gDataSet[lFlatIndex] - global::gMinValue) / (global::gMaxValue - global::gMinValue);
            }

            assert(lValue >= 0.0f && lValue <= 1.0f);
            lArray[lFlatIndexRGB] = lValue;
            lArray[lFlatIndexRGB + 1] = lValue;
            lArray[lFlatIndexRGB + 2] = lValue;
        }
    }
}

void Interpolator::simpleSibson(Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const& gDataSet)
{

    for (int xit = 0; xit < global::gScreenWidth; xit++)
    {

        for (int yit = 0; yit < global::gScreenHeight; yit++)
        {
			float lValue;
            int lFlatIndex = xit % global::gScreenWidth + global::gScreenWidth * yit;
            int lFlatIndexRGB = 3 * lFlatIndex;

            if (gDataSet[lFlatIndex] < 0.0f)
            {
                // this value needs to be interpolated
                lValue = interpolateValue(xit, yit, pVD);
                lValue = (lValue - global::gMinValue) / (global::gMaxValue - global::gMinValue);
            }
            else
            {
                // we have to value already, just copy it
                lValue = (gDataSet[lFlatIndex] - global::gMinValue) / (global::gMaxValue - global::gMinValue);
            }
            assert(lValue >= 0.0f && lValue <= 1.0f);
            lArray[lFlatIndexRGB] = lValue;
            lArray[lFlatIndexRGB + 1] = lValue;
            lArray[lFlatIndexRGB + 2] = lValue;
        }
    }
}

float Interpolator::interpolateValue(int xit, int yit, Voronoi* pVD)
{
    Point lCurrentPoint(xit, yit);
    pVD->handleRequestOverrideMembers(lCurrentPoint);

    vector<Segment> lEdges = pVD->getLastEdges();
    vector<Point> lSurroundingSites = pVD->getSurroundingSites();

    float lTop = 0;
    float lBottom = 0;

    float lDistSquared = lCurrentPoint.computeDistanceSquared(pVD->getLastSite());

    lTop += pVD->getLastSite().mValue * (1.0f / lDistSquared);
    lBottom += (1.0f / lDistSquared);

    for (auto& site : lSurroundingSites)
    {
        lDistSquared = lCurrentPoint.computeDistanceSquared(site);
        lTop += site.mValue * (1.0f / lDistSquared);
        lBottom += (1.0f / lDistSquared);
    }

	float lToBeReturned = lTop / lBottom;
	return lToBeReturned;
}

void Interpolator::areaInterpolation(Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const& gDataSet)
{
    int lFlatIndex;
    int lFlatIndexRGB;
    float lValue;

    for (int xit = 100; xit < 500; xit++)
    {
        for (int yit = 100; yit < 500; yit++)
        {
            lFlatIndex = xit % global::gScreenWidth + global::gScreenWidth * yit;
            lFlatIndexRGB = 3 * lFlatIndex;

            if (gDataSet[lFlatIndex] < 0.0f)
            {
                if (xit == 100 && yit == 201)
                {
                    continue;
                }

                // this value needs to be interpolated
                std::vector<Point> points = pVD->getOriginalPoints();
                points.push_back(Point(xit + 0.2, yit - 0.2));

                Voronoi* newVD = new Voronoi(points);

                float sumOriginal = 0;
                float sumNew = 0;

                for (int i = 0; i < points.size() - 1; i++)
                {
                    sumOriginal += pVD->getWeigths()[i];
                    sumNew += newVD->getWeigths()[i];
                }
                sumNew += newVD->getWeigths()[pVD->getOriginalPoints().size()];

                assert(abs(sumNew - sumOriginal) < 0.1);
                assert(abs(sumNew - 600 * 600) < 0.1);
                assert(abs(sumOriginal - 600 * 600) < 0.1);

                float lTop = 0.0f;
                float lBottom = 0.0f;
                for (int i = 0; i < points.size() - 1; i++)
                {
                    /*
					if (pVD->getWeigths()[i] < newVD->getWeigths()[i]) 
					{
						float lOldWeight = pVD->getWeigths()[i];
						float lNewWeight = newVD->getWeigths()[i];
						// this should not happen
						assert(1 == 0);
						global::gDebuggingInt++;
						lValue = 0;
					}
					else 
					*/
                    //if (pVD->getWeigths()[i] != newVD->getWeigths()[i])
                    {
                        float lAreaDifference = abs(pVD->getWeigths()[i] - newVD->getWeigths()[i]);
                        // area has changed, this tile is important
                        assert(lAreaDifference >= 0);
                        lTop += pVD->getOriginalPoints()[i].mValue * lAreaDifference;
                        lBottom += lAreaDifference;
                    }
                }
                float addedCell = newVD->getWeigths()[points.size() - 1];

                //assert(abs(lBottom - addedCell) < 1.0);

                if (lBottom == 0)
                {
                    //assert(1 == 0);
                    global::gDebuggingInt++;
                    lValue = 0;
                }
                else
                {
                    lValue = lTop / lBottom;
                }

                // normalize the value
                lValue = (lValue - global::gMinValue) / (global::gMaxValue - global::gMinValue);

                if (!(lValue >= 0.0f && lValue <= 1.0f))
                {
                    assert(1 == 0);
                    global::gDebuggingInt++;
                    lValue = 0;
                }
            }
            else
            {
                // we have to value already, just copy it
                lValue = (gDataSet[lFlatIndex] - global::gMinValue) / (global::gMaxValue - global::gMinValue);
                assert(lValue >= 0.0f && lValue <= 1.0f);
            }
            assert(lValue >= 0.0f && lValue <= 1.0f);
            lArray[lFlatIndexRGB] = lValue;
            lArray[lFlatIndexRGB + 1] = lValue;
            lArray[lFlatIndexRGB + 2] = lValue;
        }
    }
}

void Interpolator::idiotInterpolation(Voronoi* pVD, GLfloat* lArray, size_t lSize, std::unique_ptr<GLfloat[]> const& gDataSet)
{
    vector<int> lReference(global::gPoints.size());
    for (auto& cur : lReference)
    {
        cur = 0;
    }

#pragma omp parallel for
    for (int i = 0; i < 600; i++)
    {
#pragma omp parallel for
        for (int j = 0; j < 600; j++)
        {

			int lFlatIndex;
			int lFlatIndexRGB;
			float lValue;

            int index = -1;
            float lMinDist = 600 * 600;
			for (int it = 0; it < global::gPoints.size(); it++)
            {
                float lCurDist = global::gPoints[it].computeDistanceSquared(Point(i, j));
                if (lCurDist <= lMinDist)
                {
                    lMinDist = lCurDist;
                    index = it;
                }
            }
#pragma omp atomic
            lReference[index]++;
        }
    }

    int lReferenceSumUp = 0;
    for (int i = 0; i < lReference.size(); i++)
    {
        lReferenceSumUp += lReference[i];
    }
    assert(lReferenceSumUp == 600 * 600);

#pragma omp parallel for
    for (int xit = 0; xit < 600; xit++)
    {
#pragma omp parallel for
        for (int yit = 0; yit < 600; yit++)
        {
			float lValue;
            int lFlatIndex = xit % global::gScreenWidth + global::gScreenWidth * yit;
            int lFlatIndexRGB = 3 * lFlatIndex;

            if (gDataSet[lFlatIndex] < 0.0f)
            {
                // this value needs to be interpolated
                std::vector<Point> insertedPoints = global::gPoints;
                insertedPoints.push_back(Point(xit, yit));

                vector<int> lNewArea(insertedPoints.size());

                #pragma omp parallel for
                for (int xAgain = 0; xAgain < 600; xAgain++)
                {
                    #pragma omp parallel for
                    for (int yAgain = 0; yAgain < 600; yAgain++)
                    {
                        int index = -1;
                        float lMinDist = 600 * 600;
                        for (int it = 0; it < insertedPoints.size(); it++)
                        {
                            float lCurDist = insertedPoints[it].computeDistanceSquared(Point(xAgain, yAgain));
                            if (lCurDist <= lMinDist)  // optimization! we know the last cell is the newly added one, it will make the last cell area bigger
                            {
                                lMinDist = lCurDist;
                                index = it;
                            }
                        }
                        #pragma omp atomic
                        lNewArea[index]++;
                    }
                }

                int newSum = 0;
                for (int i = 0; i < insertedPoints.size(); i++)
                {
                    newSum += lNewArea[i];
                }
                assert(lNewArea[lNewArea.size() - 1]);
                assert(newSum == 600 * 600);

                float lTop = 0.0;
                float lBottom = 0.0;

                for (int it = 0; it < global::gPoints.size(); it++)
                {
                    float lAreaDifference = lReference[it] - lNewArea[it];
                    if (lAreaDifference < 0)
                    {
                        assert(1 == 0);
                    }
                    lTop += global::gPoints[it].mValue * lAreaDifference;
                    lBottom += lAreaDifference;
                }
                lValue = lTop / lBottom;

                // normalize the value
                lValue = (lValue - global::gMinValue) / (global::gMaxValue - global::gMinValue);

                if (lValue < 0.0f || lValue > 1.0f)
                {
                    global::gDebuggingInt++;
                    lValue = 0;
                }
            }
            else
            {
                // we have to value already, just copy it
                lValue = (gDataSet[lFlatIndex] - global::gMinValue) / (global::gMaxValue - global::gMinValue);
                assert(lValue >= 0.0f && lValue <= 1.0f);
            }
            assert(lValue >= 0.0f && lValue <= 1.0f);
            lArray[lFlatIndexRGB] = lValue;
            lArray[lFlatIndexRGB + 1] = lValue;
            lArray[lFlatIndexRGB + 2] = lValue;
        }
    }
}

void Interpolator::computeErrorArray(Voronoi* pVD, GLfloat* lArray, size_t lSize, GLfloat* pCompareMe, std::unique_ptr<GLfloat[]> const& gDataSet)
{
	vector<float> lErrorCells(global::gPoints.size());
//#pragma omp parallel for
	for (int xit = 0; xit < global::gScreenWidth; xit++)
	{
//#pragma omp parallel for
		for (int yit = 0; yit < global::gScreenWidth; yit++)
		{
			int lFlatIndex = xit % global::gScreenWidth + global::gScreenWidth * yit;
			int lFlatIndexRGB = 3 * lFlatIndex;

			float lValue;

			if (gDataSet[lFlatIndex] < 0.0f)
			{
				// this value was interpolated, it is error prone
				int lIndex = pVD->iterateVerticesReturnIndex(Point(xit, yit));
//#pragma omp atomic
				// hardcoding the function
				lErrorCells[lIndex] += abs(pCompareMe[lFlatIndexRGB] - (xit * xit + yit * yit));
			}
		}
	}

	float maxError = -1;
	float minError = 600 * 600;


	// find max and min for the error vector
	for (auto &cur : lErrorCells) 
	{
		if (cur < minError) 
		{
			minError = cur;
		}
		if (cur > maxError) 
		{
			maxError = cur;
		}
	}

#pragma omp parallel for
	for (int xit = 0; xit < global::gScreenWidth; xit++)
	{
#pragma omp parallel for
		for (int yit = 0; yit < global::gScreenWidth; yit++)
		{
			int lFlatIndex = xit % global::gScreenWidth + global::gScreenWidth * yit;
			int lFlatIndexRGB = 3 * lFlatIndex;
			
			int lIndex = pVD->iterateVerticesReturnIndex(Point(xit, yit));
			
			float lValue = (lErrorCells[lIndex] - minError) / (maxError - minError);

			// fill in the error array
			lArray[lFlatIndexRGB] = lValue;
			lArray[lFlatIndexRGB + 1] = 0.0f;
			lArray[lFlatIndexRGB + 2] = 0.0f;
		}
	}
}