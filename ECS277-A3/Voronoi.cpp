#include "Voronoi.h"
#include "global.h"

Voronoi::Voronoi(std::vector<Point> & pPoints) : mPoints(pPoints), mLastRequestPoint(-1, -1), mLastSite(-1, -1)
{
	construct_voronoi(pPoints.begin(), pPoints.end(), &mVd);

	boundary.push_back(Segment(Point(0, 0), Point(0, 600)));
	boundary.push_back(Segment(Point(0, 0), Point(600, 0)));
	boundary.push_back(Segment(Point(600, 600), Point(0, 600)));
	boundary.push_back(Segment(Point(600, 600), Point(600, 0)));

	mWeights.resize(pPoints.size());
	//weighCells();
}

// Traversing Voronoi edges using cell iterator.
std::vector<Segment> Voronoi::iterateVerticesReturnEdges(Point pP)
{
	std::vector<Segment> lToBeReturned;

	float lMinDist = 999999999.0f;

	// TODO: make this a unique pointer
	voronoi_diagram<double>::cell_type* minCell = NULL;

	int result = 0;
	for (voronoi_diagram<double>::const_cell_iterator it = mVd.cells().begin(); it != mVd.cells().end(); ++it)
	{
		const voronoi_diagram<double>::cell_type &cell = *it;
		const voronoi_diagram<double>::edge_type *edge = cell.incident_edge();

		Point lSiteOfCell = mPoints.at(cell.source_index());

		if (pP.computeDistance(lSiteOfCell) < lMinDist)
		{
			minCell = new voronoi_diagram<double>::cell_type(cell);
		}
	}

	voronoi_diagram<double>::edge_type *edge = minCell->incident_edge();

	// iterate over all edges of the cell
	do 
	{
		if (edge->is_primary()) 
		{
			auto lX = edge->vertex0()->x();
			auto lY = edge->vertex0()->y();
			Point lP0 (lX, lY);
			lX = edge->vertex0()->x();
			lY = edge->vertex0()->y();
			Point lP1(lX, lY);
			// TODO: check for fnite segments, they will fuck with me
			lToBeReturned.push_back(Segment(lP0, lP1));
		}
		edge = edge->next();
	}
	while (edge != minCell->incident_edge());

	delete minCell;

	return lToBeReturned;
}


// Traversing Voronoi edges using cell iterator.
float Voronoi::iterateVerticesReturnValue(Point pP)
{
	float lToBeReturned;

	float lMinDist = global::gScreenHeight * global::gScreenHeight;

	// TODO: make this a unique pointer
	voronoi_diagram<double>::cell_type* minCell = NULL;

	int result = 0;
	for (voronoi_diagram<double>::const_cell_iterator it = mVd.cells().begin(); it != mVd.cells().end(); ++it)
	{
		const voronoi_diagram<double>::cell_type &cell = *it;
		const voronoi_diagram<double>::edge_type *edge = cell.incident_edge();

		Point lSiteOfCell = mPoints.at(cell.source_index());

		if (pP.computeDistance(lSiteOfCell) < lMinDist)
		{
			lMinDist = pP.computeDistance(lSiteOfCell);
			minCell = new voronoi_diagram<double>::cell_type(cell);
		}
	}

	lToBeReturned = mPoints.at(minCell->source_index()).mValue;

	// clean up the pointer
	delete minCell;

	return lToBeReturned;
}

// Traversing Voronoi edges using cell iterator.
int Voronoi::iterateVerticesReturnIndex(Point pP)
{
	int lToBeReturned;

	float lMinDist = global::gScreenHeight * global::gScreenHeight;

	// TODO: make this a unique pointer
	voronoi_diagram<double>::cell_type* minCell = NULL;

	int result = 0;
	for (voronoi_diagram<double>::const_cell_iterator it = mVd.cells().begin(); it != mVd.cells().end(); ++it)
	{
		const voronoi_diagram<double>::cell_type &cell = *it;
		const voronoi_diagram<double>::edge_type *edge = cell.incident_edge();

		Point lSiteOfCell = mPoints.at(cell.source_index());

		if (pP.computeDistance(lSiteOfCell) < lMinDist)
		{
			lMinDist = pP.computeDistance(lSiteOfCell);
			minCell = new voronoi_diagram<double>::cell_type(cell);
		}
	}

	lToBeReturned = minCell->source_index();

	// clean up the pointer
	delete minCell;

	return lToBeReturned;
}

void Voronoi::handleRequestOverrideMembers(Point pP)
{
	mLastRequestPoint.setX(pP.mX);
	mLastRequestPoint.setY(pP.mY);

	float lMinDist = pow(pow(global::gScreenHeight, 2) + pow(global::gScreenWidth, 2), 0.5);

	// TODO: make this a unique pointer
	voronoi_diagram<double>::cell_type* minCell = NULL;

	int result = 0;
	for (voronoi_diagram<double>::const_cell_iterator it = mVd.cells().begin(); it != mVd.cells().end(); ++it)
	{
		const voronoi_diagram<double>::cell_type &cell = *it;
		const voronoi_diagram<double>::edge_type *edge = cell.incident_edge();

		Point lSiteOfCell = mPoints.at(cell.source_index());
		float currDistanceFromSite = pP.computeDistance(lSiteOfCell);
		if (currDistanceFromSite < lMinDist)
		{
			if ( !(mPoints.at(cell.source_index()) == mLastSite) )
			{
				minCell = new voronoi_diagram<double>::cell_type(cell);
				lMinDist = currDistanceFromSite;
			}
		}
	}

	// set last site
	mLastSite.setX(mPoints.at(minCell->source_index()).mX);
	mLastSite.setY(mPoints.at(minCell->source_index()).mY);
	mLastSite.setValue(mPoints.at(minCell->source_index()).mValue);

	voronoi_diagram<double>::edge_type *edge = minCell->incident_edge();

	// iterate over all edges of the cell, find twins and update surrounding sites

	mSurroundingSites.clear();
	do
	{
		if (edge->is_primary())
		{
			int index = edge->twin()->cell()->source_index();
			mSurroundingSites.push_back(mPoints[index]);
		}
		edge = edge->next();
	} while (edge != minCell->incident_edge());

	delete minCell;
}

void Voronoi::weighCells()
{
	/*
	I am fucking with this right now
	*/
	for (int i = 0; i < 7; i++) // mPoints.size()
	{
		vector<Point> lPolyPoints;
		handleRequestOverrideMembers(mPoints[i]);

		if (mLastEdges.size() == 6) 
		{
			for (auto& edge : mLastEdges)
			{
				lPolyPoints.push_back(edge.mP0);
			}
			mWeights[i] = abs(global::polygonArea(lPolyPoints));
		}
		else 
		{
			// we need to complete the cell first!
			// this is not trivial as order needs to be preserved

			//assert(mLastEdges.size() < 6);
			//assert(mLastEdges.size() >= 3); // not sure this check needs to be there

			for (int i = 0; i < mLastEdges.size(); i++) 
			{
				Segment current = mLastEdges[i];
				
				// always add mP1
				lPolyPoints.push_back(current.mP1);

				int possiblbeBorderIndex = global::edgeIntersection(current.mP1);
				if (possiblbeBorderIndex == -1)
				{
					// this was not a border intersection
					continue;
				}
				else 
				{
					// deal with border intersection
					Segment next = mLastEdges[(i + 1) % (mLastEdges.size())];
					int borderIndexOther = global::edgeIntersection(next.mP0);
					assert(borderIndexOther != -1);
					if (borderIndexOther == possiblbeBorderIndex) 
					{
						// we do not need to add a corner
						lPolyPoints.push_back(next.mP0);
					}
					else
					{
						// we need to add a corner
						if (possiblbeBorderIndex == 0)
						{
							if (borderIndexOther == 1)
							{
								lPolyPoints.push_back(Point(0.0, 0.0));
							}
							else if (borderIndexOther == 2)
							{
								lPolyPoints.push_back(Point(0.0, 600.0));
							}
							else
							{
								assert(1 == 0);
							}
						}
						if (possiblbeBorderIndex == 1)
						{
							if (borderIndexOther == 0)
							{
								lPolyPoints.push_back(Point(0.0, 0.0));
							}
							else if (borderIndexOther == 3)
							{
								lPolyPoints.push_back(Point(600.0, 0.0));
							}
							else
							{
								assert(1 == 0);
							}
						}
						if (possiblbeBorderIndex == 2)
						{
							if (borderIndexOther == 0)
							{
								lPolyPoints.push_back(Point(0.0, 600.0));
							}
							else if (borderIndexOther == 3)
							{
								lPolyPoints.push_back(Point(600.0, 600.0));
							}
							else
							{
								assert(1 == 0);
							}
						}
						if (possiblbeBorderIndex == 3)
						{
							if (borderIndexOther == 2)
							{
								lPolyPoints.push_back(Point(600.0, 600.0));
							}
							else if (borderIndexOther == 1)
							{
								lPolyPoints.push_back(Point(600.0, 0.0));
							}
							else
							{
								assert(1 == 0);
							}
						}
						
						// now add the next border index
						lPolyPoints.push_back(next.mP0);
					}
				}

			}

			// lPolyPoints should be fully constructed now
			// smooth sailing from now on :)

			//assert(lPolyPoints.size() == 4 || lPolyPoints.size() == 5);

			mWeights[i] = abs(global::polygonArea(lPolyPoints));
		}
	}
	if (mPoints.size() == 8)
	{
		float sum = 0;
		for (int i = 0; i < 7; i++)
		{
			sum += mWeights[i];
		}
		mWeights[7] = 600 * 600 - sum;
	}
}
