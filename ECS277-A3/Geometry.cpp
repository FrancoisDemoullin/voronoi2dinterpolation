
#include "Geometry.h"
#include "global.h"

bool Segment::isOnLine(float pX, float pY)
{
	// (b - a) cross (c - a) => checks for alignment of the 3 points
	float lCrossProduct = (pY - mP0.mY) * (mP1.mX - mP0.mX) - (pX - mP0.mX) * (mP1.mY - mP0.mY);

	if (fabs(lCrossProduct) > INTERSECTION_EPSILON)
	{
		return false;
	}

	// (b - a) dot (c - a), if negative no projection => not in bewteen
	auto lDotProduct = (pX - mP0.mX) * (mP1.mX - mP0.mX) + (pY - mP0.mY) * (mP1.mY - mP0.mY);
	if (lDotProduct < 0)
	{
		return false;
	}

	// dot product was positive, length needs to be less than sqared distance between the 2 end points
	auto lSquaredLength = (mP1.mX - mP0.mX) * (mP1.mX - mP0.mX) + (mP1.mY - mP0.mY) * (mP1.mY - mP0.mY);
	if (lDotProduct > lSquaredLength)
	{
		return false;
	}

	// all test passed, return true
	return true;
}

float Segment::getSlope()
{
	if ((mP1.mX - mP0.mX) == 0)
	{
		return INFINITY;
	}
	// rise over run. Overly casted! I am sorry, just making sure...
	return (float)((float)(mP1.mY - mP0.mY) / (float)(mP1.mX - mP0.mX));
}

Point Line::computeIntersection(Line pOther)
{
	float lX;
	float lY;

	if (pOther.isInf)
	{
		lX = pOther.mP0.mX;
		lY = mSlope * lX + mB;
	}
	else if (isInf)
	{
		lX = mP0.mX;
		lY = pOther.mSlope * lX + pOther.mB;
	}
	else
	{
		lX = (mB - pOther.mB) / (pOther.mSlope - mSlope);
		lY = mSlope * lX + mB;
	}

	return Point(lX, lY);
}

vector<Point> Line::computeIntersections(vector<Segment>& pSeg, vector<int>& pEdgeIndexVector)
{
	vector<Point> lToBeReturned;

	// computes the area of the sub-area
	int indexTracker = 0;

	for (auto& currSeg : pSeg)
	{
		float lSlope = currSeg.getSlope();
		bool degenerate = false;
		if (lSlope == INFINITY)
		{
			degenerate = true;
		}
		Line lOther(currSeg.mP1, lSlope, degenerate);

		Point lCandidate;
		if (mSlope != lOther.mSlope)
		{
			Point lTemp = computeIntersection(lOther);
			lCandidate.setX(lTemp.mX);
			lCandidate.setY(lTemp.mY);
		}
		else
		{
			lCandidate.setX(mP0.mX);
			lCandidate.setY(mP0.mY);
		}
		if (currSeg.isOnLine(lCandidate.mX, lCandidate.mY) && lCandidate.mX >= 0 && lCandidate.mY >= 0)
		{
			// we found a valid intersection
			lToBeReturned.push_back(lCandidate);
			pEdgeIndexVector.push_back(indexTracker);
		}
		else
		{
			int a = 0;
		}
		indexTracker++;
	}

	return lToBeReturned;
}

vector<Point> Line::computeBoundaryIntersections()
{
	vector<Point> lToBeReturned;

	// computes the area of the sub-area
	int indexTracker = 0;

	for (auto& currSeg : global::gBoundary)
	{
		float lSlope = currSeg.getSlope();
		bool degenerate = false;
		if (lSlope == INFINITY)
		{
			degenerate = true;
		}
		Line lOther(currSeg.mP1, lSlope, degenerate);

		Point lCandidate;
		if (mSlope != lOther.mSlope)
		{
			Point lTemp = computeIntersection(lOther);
			lCandidate.setX(lTemp.mX);
			lCandidate.setY(lTemp.mY);
		}
		else
		{
			lCandidate.setX(mP0.mX);
			lCandidate.setY(mP0.mY);
		}
		switch (indexTracker) 
		{
		case 0:
			if (-0.01 < lCandidate.mX && 0.01 > lCandidate.mX)
			{
				if (0.0 <= lCandidate.mY && 600.0 >= lCandidate.mY)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		case 1:
			if (-0.01 < lCandidate.mY && 0.01 > lCandidate.mY)
			{
				if (0.0 <= lCandidate.mX && 600.0 >= lCandidate.mX)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		case 2:
			if (599.99 < lCandidate.mY && 600.01 > lCandidate.mY)
			{
				if (0.0 <= lCandidate.mX && 600.0 >= lCandidate.mX)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		case 3:
			if (599.99 < lCandidate.mX && 600.01 > lCandidate.mX)
			{
				if (0.0 <= lCandidate.mY && 600.0 >= lCandidate.mY)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		default:
			assert(1 == 0);
		}
		indexTracker++;
	}

	return lToBeReturned;
}