#include <iostream>
#include <cstdio>
#include <vector>
#include <memory>
#include <ctime>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Geometry.h"
#include "Voronoi.h"
#include "global.h"
#include "SliceRenderer.h"
#include "Interpolator.h"

/*
 * Overriding the global namespace variables
*/
int global::gScreenHeight = 600;
int global::gScreenWidth = 600;
float global::gMaxValue = 0.0f;
float global::gMinValue = 0.0f;
int global::gDebuggingInt = 0;
int global::gMode = 0;

vector<Segment> global::gBoundary{ Segment(Point(0, 0), Point(0, 600)), Segment(Point(0, 0), Point(600, 0)),Segment(Point(0, 600), Point(600, 600)), Segment(Point(600, 0), Point(600, 600))};
vector<Point> global::gPoints;

/*
User input:
Function prototypes
Keys array allows multiple keyes to be pressed simultaneously
*/
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void executeInput();
bool keys[1024];

// Pointers

std::unique_ptr<SliceRenderer> gSliceRenderer;
std::unique_ptr<Voronoi> lVD;
std::unique_ptr<GLfloat[]> gDataSet(new GLfloat[global::gScreenWidth * global::gScreenHeight]);


// raw look at pointers for the arrays
GLfloat* gCellInterpolation = new GLfloat[global::gScreenWidth * global::gScreenHeight * 3];
GLfloat* gEasySibson = new GLfloat[global::gScreenWidth * global::gScreenHeight * 3];
GLfloat* gSibson = new GLfloat[global::gScreenWidth * global::gScreenHeight * 3];

GLfloat* gError = new GLfloat[global::gScreenWidth * global::gScreenHeight * 3];

size_t gSize = global::gScreenWidth * global::gScreenHeight * 3;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    GLFWwindow* window = glfwCreateWindow(global::gScreenWidth, global::gScreenHeight, "ECS277 - A2", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    // Set the required callback functions
    glfwSetKeyCallback(window, key_callback);

    // Options
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Initialize GLEW to setup the OpenGL Function pointers
    glewExperimental = GL_TRUE;
    glewInit();

    // Define the viewport dimensions
    glViewport(0, 0, global::gScreenHeight, global::gScreenWidth);

    // Setup some OpenGL options
    glEnable(GL_DEPTH_TEST);

	float PERCENTAGE = 0.1f;
	assert(PERCENTAGE > 0.0f && PERCENTAGE < 1.0);

	// fill the dataSet
	for (int i = 0; i < 7; i++)
	{
		int x = rand() % 599;
		int y = rand() % 599;
		float value = x * x + y * y;
		global::gPoints.push_back(Point(x, y, value));
	}

	// compute Max and Min of data set (somewhat inefficient)
	global::computeMaxMin(global::gPoints);

	// setting up the dataSet
	global::initializeDataSet(gDataSet.get(), global::gPoints);

    // initialize unique_ptr s
    gSliceRenderer.reset(new SliceRenderer());
    lVD.reset(new Voronoi(global::gPoints));

	// compute the texture arrays - timed
	std::clock_t start;
	double duration;
	start = std::clock();

	Interpolator::computeTextureArray(0, lVD.get(), gCellInterpolation, gSize, gDataSet);

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "cell interpolation: " << duration << endl;
	start = std::clock();

	Interpolator::computeTextureArray(1, lVD.get(), gEasySibson, gSize, gDataSet);

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "simplified Sibson: " << duration << endl;
	start = std::clock();

	Interpolator::computeTextureArray(2, lVD.get(), gSibson, gSize, gDataSet);

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "Sibson: " << duration << endl;

	start = std::clock();

	Interpolator::computeErrorArray(lVD.get(), gError, gSize, gEasySibson, gDataSet);

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "Sibson: " << duration << endl;

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        executeInput();

        // Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		switch (global::gMode) 
		{
		case 0:
			gSliceRenderer->updateTexture(gCellInterpolation);
			break;
		case 1:
			gSliceRenderer->updateTexture(gEasySibson);
			break;
		case 2:
			gSliceRenderer->updateTexture(gSibson);
			break;
		case 3:
			gSliceRenderer->updateTexture(gError);
			break;
		default:
			assert(1 == 0);
		}
        
		// render the texture
        gSliceRenderer->renderTexture();

        // Swap the buffers
        glfwSwapBuffers(window);

        //cout << "Screen" << endl;
    }

    glfwTerminate();

    return 0;
}

// executes the user input
void executeInput()
{
    if (keys[GLFW_KEY_0])
    {
        global::gMode = 0;
    }
    if (keys[GLFW_KEY_1])
    {
		global::gMode = 1;
    }
    if (keys[GLFW_KEY_2])
    {
		global::gMode = 2;
    }
	if (keys[GLFW_KEY_3])
	{
		global::gMode = 3;
	}
}

// this is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
        {
            keys[key] = true;
        }
        else if (action == GLFW_RELEASE)
        {
            keys[key] = false;
        }
    }
}